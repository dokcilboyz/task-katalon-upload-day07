<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suite Gambar</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>ec24b32b-65c9-45d7-81fb-24ec7b9cb1a5</testSuiteGuid>
   <testCaseLink>
      <guid>44ef6803-64bf-49e3-a7bc-4c30254d5739</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TC01 - Gambar 1</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>84bc371e-8f5c-4fff-b0f6-18e61a86c318</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <iterationNameVariable>
         <defaultValue>''</defaultValue>
         <description></description>
         <id>b13aa226-3b99-4b3a-8725-291c8c9b0759</id>
         <masked>false</masked>
         <name>Link</name>
      </iterationNameVariable>
      <testCaseId>Test Cases/TC02 - Gambar 2</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>32a05259-b2d4-481a-8205-9ea49ddfa8ae</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Data Gambar</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>32a05259-b2d4-481a-8205-9ea49ddfa8ae</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Link</value>
         <variableId>b13aa226-3b99-4b3a-8725-291c8c9b0759</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>619aedcd-438b-4b36-b3e7-3d8a046a63ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TC03 - Gambar 3</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
